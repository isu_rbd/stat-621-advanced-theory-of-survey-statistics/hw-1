---
title: "STAT 621 | HW 1"
author: "Ricardo Batista"
date: "2/2/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Question 4

&nbsp;&nbsp;&nbsp;&nbsp; We estimate $\theta = \sum_{h = 1}^H \alpha_h \bar{y}_{N_h}$ by $\hat \theta = \sum_{h = 1}^H \alpha_h \bar{y}_h$ where

$$
\begin{aligned}
\bar{y}_h &= \sum_{i \in A_h} y_{hi}
\\[10pt]
V(\bar{y}_h) &= \left(1 - \frac{n_h}{N_h}\right) \frac{S_{h}^2}{n_h} 
\hspace{50pt} S_{h}^2 = \frac{1}{N_h - 1} \sum_{i = 1}^{N_h}\left(y_{hi} - \bar{y}_{N_h}\right)^2
\\[10pt]
\hat{V}(\bar{y}_h) &= \left(1 - \frac{n_h}{N_h}\right) \frac{s_{h}^2}{n_h} 
\hspace{50pt} s_{h}^2 = \frac{1}{n_h - 1} \sum_{i \in A_h}\left(y_{hi} - \bar{y}_h\right)^2
\\[10pt]
V(\hat \theta)
&=
\sum_{h = 1}^H \alpha_h^2 \left(1 - \frac{n_h}{N_h}\right)\frac{S_{h}^2}{n_h}
=
\sum_{h = 1}^H \frac{\alpha_h^2 S_{h}^2}{n_h} - \sum_{h = 1}^H \frac{\alpha_h^2 S_{h}^2}{N_h}
= \sum_{h = 1}^H \frac{A_h}{n_h} + B
\end{aligned}
$$

(Note we've assumed a finite population.) We seek to minimize $V(\hat\theta)$ subject to the cost constraint $C = C_0 - c \sum_{h = 1}^Hn_h$ via the method of Lagrange multipliers. As such, given Lagrange function

$$
\begin{aligned}
\mathcal{L}((n_1, ... n_H), \lambda) 
&= V(\hat \theta) + \lambda\left(C - C_0 - \sum_{h = 1}^H c n_h\right)
\\[10pt]
&=\sum{A_h}{n_h} + B + \lambda\left(C - C_0 - c \sum_{h = 1}^H n_h\right),
\end{aligned}
$$

we compute its gradients and solve for $n_h$

$$
\begin{aligned}
\frac{\partial \mathcal{L}((n_1, ... n_H), \lambda) }{\partial n_h} &= - \frac{A_h}{n_h^2} - \lambda c
&& \implies \hspace{20pt}
n_h = - \sqrt{\frac{A_h}{c\lambda}}
\\[10pt]
\frac{\partial \mathcal{L}((n_1, ... n_H), \lambda) }{\partial \lambda} &= C - C_0 - c \sum_{h = 1}^N n_h
&& \implies \hspace{20pt}
-\sqrt{\lambda} = \frac{\sqrt{c} \sum_{h = 1}^H \sqrt{A_h}}{C - C_0}
\end{aligned}
$$

to obtain

$$
n_h = \sqrt{\frac{A_h}{c}} \left(-\sqrt{\lambda}\right)^{-1}
= \left(\frac{C - C_0}{c \sum_{h = 1}^H \alpha_h S_h}\right)\alpha_h S_h.
$$

In practice, we replace $S_h$ by its estimate $s_h$.  

# Question 7

&nbsp;&nbsp;&nbsp;&nbsp; The $k$ possible samples partition the population into $k$ groups

$$
U = U_1 \cup U_2 \cup \cdots \cup U_k.
$$

For each possible random start $r \in 1, ..., k$ define

$$
t_{U_r} = \sum_{i \in U_r} y_i \quad \text{and} \quad I_{U_r} = I[A = U_r].
$$

So $\bar{y}_N= \frac{1}{N} \sum_{r = 1}^k t_{U_r}$. 

&nbsp;&nbsp;&nbsp;&nbsp; By p.23 in the text, we know that $L$ and $k - L$ of the samples are of size $q + 1$ and $q$, respectively. As such,

$$
\bar{y}_n 
=
\begin{cases}
\frac{1}{q + 1}t_{U_r} &\text{if } A = U_r \text{ s.t. } |U_r| = q + 1\\[10pt]
\frac{1}{q}t_{U_r} &\text{if } A = U_r \text{ s.t. } |U_r| = q
\end{cases}
$$

and thus

$$
\mathbb{E}[\bar{y}_n]
= 
\sum_{j = 1}^L\frac{t_{U_j}}{q + 1}  \frac{1}{k} + \sum_{h = 1}^{k - L}\frac{t_{U_h}}{q }  \frac{1}{k}
=
\frac{1}{k}\left[\frac{1}{q + 1}\sum_{j = 1}^Lt_{U_j} + \frac{1}{q} \sum_{h = 1}^{k - L} t_{U_h}\right].
$$

where $j = 1, ..., L$ and $h = 1, ..., k - L$ index the samples with $q + 1$ and $q$ elements, respectively. As a consequence, 

$$
\begin{aligned}
\text{Bias} (\bar{y}_n)
= 
\mathbb{E}[\bar{y}_n] - \bar{y}_N
&=
\frac{1}{k}\left[\frac{1}{q + 1}\sum_{j = 1}^Lt_{U_j} + \frac{1}{q} \sum_{h = 1}^{k - L} t_{U_h}\right] - \frac{1}{N} \sum_{r = 1}^k t_{U_r}
\\[10pt]
&=
\sum_{j = 1}^L \frac{L - k}{k(q + 1)N}t_{U_j} + \sum_{h = 1}^{k - L} \frac{L}{kqN} t_{U_h}
\\[10pt]
&=
\frac{1}{kN}\left[\sum_{j = 1}^L \frac{L - k}{q + 1}t_{U_j} + \sum_{h = 1}^{k - L} \frac{L}{q} t_{U_h}\right]
\end{aligned}
$$

where we used the fact that $N = kq + L$.

&nbsp;&nbsp;&nbsp;&nbsp; Consider now the sampling design $P(A) = |A|/N$ for the sample mean. Then,

$$
\mathbb{E}_P[\bar{y}_n]
= 
\sum_{j = 1}^L\frac{t_{U_j}}{q + 1}  \frac{q + 1}{N} + \sum_{h = 1}^{k - L}\frac{t_{U_h}}{q }  \frac{q}{N}
=
\sum_{j = 1}^L\frac{t_{U_j}}{N} + \sum_{h = 1}^{k - L}\frac{t_{U_h}}{N} 
=
\frac{1}{N}\sum_{r = 1}^Nt_{U_r} = \bar{y}_N
$$

That is, the sample mean is unbiased under the foregoing sample design.  

# Question 11

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; Consider the ordered sample $OS = (a_1, ..., a_n)$ where $a_k \in (1, R)$ is the person associated with the selected row. Then we have

$$
\hat R 
= \sum_{k = 1}^n \frac{Z_k}{p_{a_k}}
= \sum_{i \in A} \frac{N}{n} \frac{t_i}{r_i}
$$
where $p_{a_k} = n/N$ and 

$$
Z_k = \frac{1}{r_{a_k}}= \sum_{i = 1}^R \frac{1}{r_i} I(a_k = i).
$$

Note $\hat R$ is unbiased:

$$
\mathbb{E} [\hat {R}]
= \mathbb{E} \left[\sum_{k = 1}^N I_k \frac{1}{r_i} \frac{N}{n}\right] 
= \sum_{k = 1}^N \frac{n}{N} \frac{1}{r_i} \frac{N}{n}
= r_1\frac{1}{r_1} + \cdots + r_R\frac{1}{r_R}
= R
$$

## (b)

&nbsp;&nbsp;&nbsp;&nbsp; The estimator for the total of y is similar to the one above:

$$
\hat T_y
= \sum_{k = 1}^n \frac{Z_k}{p_{a_k}}
= \sum_{i \in A} \frac{N}{n} t_i y_i
$$

where $p_{a_k} = n/N$ and 

$$
Z_k = y_{a_k}= \sum_{i = 1}^R y_{i} I(a_k = i).
$$

Note $\hat T_y$ is unbiased:

$$
\mathbb{E} [\hat T_y]
= \mathbb{E} \left[\sum_{k = 1}^N I_k y_i \frac{N}{n}\right] 
= \sum_{k = 1}^N \frac{n}{N} y_i \frac{N}{n}
= r_1 y_1 + \cdots + r_Ry_R
$$


# Question 13

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; When drawing one of the $n$ integers from the set $\{1, 2, ..., M_N\}$,

$$
p_i = \frac{M_i - M_{i - 1}}{M_N}
$$

is the probability that element $i$ will selected in that single draw.

### i.

$$
\begin{pmatrix} n \\ 1 \end{pmatrix} p_i (1 - p_i)^{n - 1}
$$

### ii.

$$
1 - (1 - p_i)^{n}
$$

### iii.

&nbsp;&nbsp;&nbsp;&nbsp; Consider the events

$$
A : \text{element } i \text{ appears in the sample}\\[10pt]
B : \text{element } k \text{ appears in the sample}.
$$

Assuming $i \neq k$, then

$$
\begin{aligned}
P(A \cap B) 
= 1 - P(A^c \cup B^c)
&= 1 - \left[P(A^c) + P(B^c) - P(A^c \cap B^c)\right]
\\[10pt]
&= 1 - \left[(1 - p_i)^{n} + (1 - p_k)^{n} - (1 - (p_i + p_k))^{n}\right]
\end{aligned}
$$

## (b)

### i.

$$
\frac{\begin{pmatrix} M_N - m_i \\ n - 1\end{pmatrix}}{\begin{pmatrix} M_N\\ n\end{pmatrix}}
$$

### ii.

$$
\frac{\begin{pmatrix} M_N\\ n\end{pmatrix} - \begin{pmatrix} M_N - m_i \\ n \end{pmatrix}}{\begin{pmatrix} M_N\\ n\end{pmatrix}}
$$

### iii.


$$
\frac{\begin{pmatrix} M_N\\ n\end{pmatrix} - \begin{pmatrix} M_N - m_i \\ n \end{pmatrix} - \begin{pmatrix} M_N - m_k \\ n \end{pmatrix} + \begin{pmatrix} M_N - m_i - m_k \\ n \end{pmatrix}}{\begin{pmatrix} M_N\\ n\end{pmatrix}}
$$

\pagebreak

# Question 14

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\text{Preliminary results}}:$

&nbsp;&nbsp;&nbsp;&nbsp; We're told "$x_i$ is distributed as a multiple of a chi-square random variable," so

$$
\begin{aligned}
x_i &\sim k\chi_d^2 = \chi_{kd}^2
\\[10pt]
\frac{1}{x_i} &\sim \text{Inv-}\chi_{kd}^2
\end{aligned}
$$
As such,

$$
\begin{aligned}
\mathbb{E}[x_i] &= kd
\\[10pt]
\mathbb{E}[x_i^2] &= Var(x_i) + \mathbb{E}[x_i]^2 = 2kd + (kd)^2 = kd(2 + kd)
\\[10pt]
\mathbb{E}\left[\frac{1}{x_i}\right] &= \frac{1}{kd - 2}
\\[10pt]
\mathbb{E}\left[\left(\frac{1}{x_i}\right)^2\right] &= \frac{2}{(kd - 2)^2(kd - 4)} + \frac{1}{(kd - 2)^2}
                                                     = \frac{1}{(kd - 2)(kd - 4)}
\\[10pt]
\mathbb{E}[y_i] &= \mathbb{E}[\mathbb{E}[\beta_0 + \beta_1 x_i + e_i |x_i]] 
                 = \mathbb{E}[\beta_0 + \beta_1 x_i]  = \beta_0 + \beta_1 kd
\\[10pt]
\mathbb{E}[x_i y_i]  &= \mathbb{E}[\mathbb{E}[x_i(\beta_0 + \beta_1 x_i + e_i) |x_i]] 
                      = \mathbb{E}[\beta_0 x_i + \beta_1 x_i^2 ]
                      = kd(\beta_0 + \beta_1(2 + kd))
\\[10pt]
\mathbb{E}\left[\frac{y_i}{x_i}\right] &= \mathbb{E}\left[\mathbb{E}\left[\left. \frac{\beta_0 + \beta_1x_i + e_i}{x_i}\right| x_i\right]\right]
                                        = \mathbb{E}\left[\frac{\beta_0}{x_i} + \beta_1\right]
                                        = \frac{\beta_0}{kd - 2} + \beta_1
\\[10pt]
\mathbb{E}\left[\frac{y_i}{x_i^2}\right] &= \mathbb{E}\left[\mathbb{E}\left[\left. \frac{\beta_0 + \beta_1x_i + e_i}{x_i}\right| x_i^2\right]\right]
                                          = \mathbb{E}\left[\frac{\beta_0}{x_i^2} + \frac{\beta_1}{x_i}\right]
                                          = \frac{1}{kd - 2}\left(\frac{\beta_0}{kd - 4} + \beta_1\right) 
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\text{Calculations}}:$

&nbsp;&nbsp;&nbsp;&nbsp; *First element*:

$$
\begin{aligned}
\mathbb{E} \left[ \left(\sum_{i = 1}^N x_i\right)\left(\sum_{i = 1}^N \frac{I_i}{x_i}\right)\right]
&=
\mathbb{E} \left[ \sum_{i = 1}^N I_i + \sum_{i \neq j}^N I_i\frac{x_j}{x_i}\right]
=
\sum_{i = 1}^N \mathbb{E}[I_i] + \sum_{i \neq j}^N \mathbb{E}\left[I_i\frac{x_j}{x_i}\right]
\\[10pt]
&=
\sum_{i = 1}^N \pi_i + \frac{kd}{kd - 2}\sum_{i \neq j}^N \pi_i
=
n_B + \frac{kd}{kd - 2}2n_B
\\[10pt]
&=
n_B\frac{3kd - 2}{kd - 2}
\end{aligned}
$$

where we used the fact that $I_i$, $x_i^{-1}$, and $x_j$ are mutually independent.  

&nbsp;&nbsp;&nbsp;&nbsp; *Second element*:

$$
\begin{aligned}
\mathbb{E} \left[ \left(\sum_{i = 1}^N x_i\right)\left(\sum_{i = 1}^N I_iy_i\right)\right]
&=
\mathbb{E} \left[ \sum_{i = 1}^N I_ix_iy_i + \sum_{i \neq j}^N I_iy_ix_j\right]
\\[10pt]
&=
\sum_{i = 1}^N\mathbb{E}[I_ix_iy_i] + \sum_{i \neq j}^N \mathbb{E}\left[I_iy_ix_j\right]
\\[10pt]
&=
kd(\beta_0 + \beta_1(2 + kd))\sum_{i = 1}^N \pi_i + kd(\beta_0 + \beta_1kd)\sum_{i \neq j}^N \pi_i
\\[10pt]
&=
2n_Bkd(\beta_0 + \beta_1(1 + kd))
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; *Third element*:

$$
\begin{aligned}
\mathbb{E} \left[ \left(\sum_{i = 1}^N x_i\right)\left(\sum_{i = 1}^N I_i\frac{y_i}{x_i^2}\right)\right]
&=
\mathbb{E} \left[ \sum_{i = 1}^N I_i \frac{y_i}{x_i} + \sum_{i \neq j}^N I_i\frac{y_i}{x_i^2}x_j\right]
=
\sum_{i = 1}^N \mathbb{E}\left[I_i \frac{y_i}{x_i}\right] + \sum_{i \neq j}^N \mathbb{E}\left[I_i\frac{y_i}{x_i^2}x_j\right]
\\[10pt]
&=
\left(\frac{\beta_0}{kd - 2} + \beta_1\right)\sum_{i = 1}^N \pi_i + kd \left(\frac{1}{kd - 2}\left(\frac{\beta_0}{kd - 4} + \beta_1\right) \right)\sum_{i \neq j}^N \pi_i
\\[10pt]
&=
n_B \cdot \frac{(3kd - 4) \beta_0 + (kd - 4)(3kd - 2)\beta_1}{(kd - 2)(kd - 4)}
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; *Fourth element*:

$$
\begin{aligned}
\mathbb{E} \left[ \left(\sum_{i = 1}^N x_i\right)\left(\sum_{i = 1}^N I_i\frac{y_i}{x_i}\right)\right]
&=
\mathbb{E} \left[ \sum_{i = 1}^N I_i y_i + \sum_{i \neq j}^N I_i\frac{y_i}{x_i}x_j\right]
=
\sum_{i = 1}^N \mathbb{E}\left[I_i y_i\right] + \sum_{i \neq j}^N \mathbb{E}\left[I_i\frac{y_i}{x_i}x_j\right]
\\[10pt]
&=
(\beta_0 + kd\beta_1)\sum_{i = 1}^N \pi_i + kd \left(\frac{\beta_0}{kd - 2} + \beta_1\right) \sum_{i \neq j}^N \pi_i
\\[10pt]
&=
n_B \cdot \frac{(3kd - 2)\beta_0 + 3kd(kd - 2)\beta_1}{kd - 2}
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; *Final result*:

&nbsp;&nbsp;&nbsp;&nbsp; The expected value of the foregoing vector is

$$
\begin{aligned}
\left(
\frac{3kd - 2}{kd - 2}, \quad
2kd(\beta_0 + \beta_1(1 + kd)), \quad
\frac{(3kd - 4) \beta_0 + (kd - 4)(3kd - 2)\beta_1}{(kd - 2)(kd - 4)}, \quad
\frac{(3kd - 2)\beta_0 + 3kd(kd - 2)\beta_1}{kd - 2}
\right)
\end{aligned}
$$

\pagebreak

# Question 26

&nbsp;&nbsp;&nbsp;&nbsp; Let $\epsilon > 0$ be given. Since $E[X_n] = 0$, $V_n(X_n) = E[X_n^2] =  O_p(n^{-\alpha})$, so there exists $N_\varepsilon$ such that

$$
\begin{aligned}
P\left(\frac{E[X_n^2]}{n^{-\alpha}} > T_\varepsilon\right)
=
P\left(E[X_n^2] > T_\varepsilon n^{-\alpha}\right)
< \varepsilon, \quad \forall n > N_\varepsilon
&& (1)
\end{aligned}
$$

Note $E[X_n^2]$ is a degenerate random variable, so $P\left(E[X_n^2] > T_\varepsilon n^{-\alpha}\right) \in \{0, 1\}$. Since (1) holds for all $\varepsilon > 0$, it holds for $\varepsilon \in (0, 1)$, so $E[X_n^2]$ must be finite. 

&nbsp;&nbsp;&nbsp;&nbsp; Let 

$$
M_\varepsilon = \sqrt{\frac{E[X_n^2]}{n^{-\alpha}\varepsilon}}
$$

By Chebyshev's inequality,

$$
P\left(|X_n| > M_\varepsilon n^{-0.5 \alpha}\right) \leq \frac{E[X_n^2]}{M_\varepsilon^2 n^{-\alpha}} \leq \varepsilon.
$$


# Question 29

&nbsp;&nbsp;&nbsp;&nbsp; First, we have that

$$
\begin{aligned}
2N\sum_{i \in U} (y_i - \bar y_N)^2
&=
2N\sum_{i \in U} \left( y_i^2 - 2y_i\bar y_N + \bar y_N^2\right)
\\[10pt]
&=
2N\sum_{i \in U} y_i^2 - 4\sum_{i \in U}\sum_{j \in U}y_iy_j + 2N^2\left(\frac{1}{N} \sum_{i \in U} y_i\right)^2
\\[10pt]
&=
2N\sum_{i \in U} y_i^2 - 4\sum_{i \in U}\sum_{j \in U}y_iy_j + 2\sum_{i \in U}\sum_{j \in U}y_iy_j
\\[10pt]
&=
2N\sum_{i \in U} y_i^2 - 2\sum_{i \in U}\sum_{j \in U}y_iy_j
\\[10pt]
&=
N\sum_{i \in U} y_i^2 - 2\sum_{i \in U}\sum_{j \in U}y_iy_j + N\sum_{j \in U} y_i^2
\\[10pt]
&=
\sum_{i \in U}\sum_{j \in U} y_i^2 - 2\sum_{i \in U}\sum_{j \in U}y_iy_j + \sum_{i \in U}\sum_{j \in U} y_i^2
\\[10pt]
&=\sum_{i \in U}\sum_{j \in U} \left(y_i^2 - 2y_iy_j + y_j^2\right)
\\[10pt]
&=\sum_{i \in U}\sum_{j \in U} \left(y_i -y_j\right)^2
\end{aligned}
$$
Then,

$$
\begin{aligned}
S^2
&=
\frac{1}{N - 1} \sum_{i \in U} (y_i - \bar y_N)^2
=
\frac{1}{2N(N - 1)} 2N\sum_{i \in U} (y_i - \bar y_N)^2
=
\frac{1}{2N(N - 1)} \sum_{i \in U}\sum_{j \in U} \left(y_i -y_j\right)^2.
\end{aligned}
$$

Finally, we can express (1.2.28) as


$$
\begin{aligned}
\frac{1}{2} \sum_{i \in U}\sum_{j \in U} (\pi_i\pi_k - \pi_{ik})\left(\frac{y_i}{\pi_i} - \frac{y_k}{\pi_k}\right)^2
&=
\frac{1}{2} \sum_{i \in U}\sum_{j \in U} \left(\frac{n^2}{N^2} - \frac{n(n - 1)}{N(N - 1)}\right)\left(\frac{Ny_i}{n} - \frac{Ny_k}{n}\right)^2
\\[10pt]
&=
\frac{1}{2} \left(\frac{nN - n - Nn + N}{N(N - 1)}\right) \frac{N}{n} \left[2N(N-1)S^2\right]
\\[10pt]
&=
\frac{N}{n}(N - n)S^2
\\[10pt]
&=
N^2\left(\frac{1}{n} - \frac{1}{N}\right)S^2
\end{aligned}
$$


References: 
- https://stats.stackexchange.com/questions/398635/relation-between-pairwise-distance-sum-and-sum-of-distance-to-mean-gap-statisti



# Question 31

&nbsp;&nbsp;&nbsp;&nbsp; Let $Z_n$ denote the random variable of smaller order in probability; that is

$$
\hat\theta_n = B_n + o_p(|\hat{\theta}|) = B_n + Z_n.
$$
We're given that $Z_n = o_p(|\hat{\theta}|)$, i.e., 

$$
\begin{aligned}
&\lim_{n \rightarrow \infty} P\left( \left|\frac{Z_n}{\hat \theta_n}\right| > \delta \right) = 0
\\[15pt]
\iff \quad & \forall \varepsilon, \delta >0, \exists N_{\varepsilon, \delta} \text{ such that}
\\[5pt]
& P\left( \left|\frac{Z_n}{\hat \theta_n}\right| > \delta \right) < \varepsilon
\end{aligned}
$$

Now, let $\varepsilon, \delta > 0$ be given. Then,


$$
\begin{aligned}
\varepsilon >
P\left( \left|\frac{Z_n}{\hat \theta_n}\right| > \delta \right)
& = 
P\left( \left|\frac{\hat \theta_n - B_n}{\hat \theta_n}\right| > \delta \right)
=
P\left( \left| 1 - \frac{B_n}{\hat \theta_n}\right| > \delta \right)
=
P\left( \left\{\frac{B_n}{\hat{\theta}_n} < 1 - \delta\right\} \bigcup \left\{\frac{B_n}{\hat{\theta}_n} > 1 + \delta\right\}  \right)
\\[10pt]
&=
P\left( \left\{\frac{1}{1 - \delta} < \frac{\hat{\theta}_n}{B_n} < 0\right\} \bigcup \left\{\frac{1}{1 + \delta} > \frac{\hat{\theta}_n}{B_n} > 0\right\}  \right)
\\[10pt]
&>
P\left( \left\{-\frac{1}{1 + \delta} < \frac{\hat{\theta}_n}{B_n} < 0\right\} \bigcup \left\{\frac{1}{1 + \delta} > \frac{\hat{\theta}_n}{B_n} > 0\right\}  \right)
\\[10pt]
&=
P\left( \left|\frac{\hat\theta_n}{B_n}\right| > \frac{1}{1 + \delta} \right)
\end{aligned}
$$

revels that $\hat\theta_n = O_p(|B_n|)$ (where we've set $M_\varepsilon = \frac{1}{1 + \delta}$).  

# Question 44


&nbsp;&nbsp;&nbsp;&nbsp; From STAT 521, we know (1.2.66) is the $pwr$ (p-expanded with replacement) estimator of the population total. The $pwr$ estimator of the mean for a simple random sample (of size $n = 2$) with replacement (from a population of size $N = 4$)

$$
\hat y_{pwr} 
= \frac{1}{N}\hat{t}_{pwr}
= \frac{1}{N}\left[\sum_{i \in A} \frac{t_i}{np_i}y_i\right]
= \frac{1}{N}\left[\sum_{i \in A} \frac{Nt_i}{n}y_i\right]
= \sum_{i \in A} \frac{t_i}{n}y_i
= \sum_{i \in A} \frac{t_i}{2}y_i
$$

and its variance is

$$
V(y_{pwr}) = \frac{1}{n} \sigma^2 = \frac{1}{2} \sigma^2, \quad\quad
\sigma^2 = \frac{1}{N}\sum_{i = 1}^N(y_i - \bar{y}_N)^2
= \frac{1}{4}\sum_{i = 1}^4(y_i - \bar{y}_N)^2
$$

&nbsp;&nbsp;&nbsp;&nbsp; Now, note estimator (1.2.71) -- which we label $\hat y_{new}$ is unbiased

$$
\mathbb{E}[\hat y_{new}]
=
\left(\frac{1}{4}\right)^2 \sum_{i = 1}^4 y_i + 2\left(\frac{1}{4}\right)^2 \sum_{j > i}^4 \frac{y_i + y_j}{2}
=
\frac{1}{16}\left[\sum_{i = 1}^4 y_i + 3\sum_{i = 1}^4 y_i\right] = \bar{y}_N.
$$

To calculate $V(\hat y_{new})$ we make use of $V_{SI}(\bar y)$, the variance of the sample mean under simple random sample without replacement (for a sample of size $n = 2$ from a population of size $N = 4$):

$$
\begin{aligned}
V_{SI}(\bar y) 
&= \left(1 - \frac{n}{N}\right)\frac{S^2}{n} 
 = \left(1 - \frac{2}{4}\right)\frac{S^2}{2}
 = \frac{S^2}{4}
\\[10pt]
V_{SI}(\bar y)
&= \begin{pmatrix}4 \\ 2\end{pmatrix}^{-1} \sum_{j > i}^4 \left(\frac{y_i + y_j}{2} - \bar{y}_N\right)^2
 = \frac{1}{6} \sum_{j > i}^4 \left(\frac{y_i + y_j}{2} - \bar{y}_N\right)^2
\end{aligned}
$$
where

$$
S^2 = \frac{1}{N - 1} \sum_{i = 1}^{N} (y_i - \bar{y}_N)^2 
    = \frac{1}{4 - 1} \sum_{i = 1}^{4} (y_i - \bar{y}_N)^2
    = \frac{1}{3} \sum_{i = 1}^{4} (y_i - \bar{y}_N)^2.
$$

Then,

$$
\begin{aligned}
V(\hat y_{new})
&= \left(\frac{1}{4}\right)^2 \sum_{i = 1}^4 (y_i - \bar{y}_N)^2 + 
 2\left(\frac{1}{4}\right)^2  \sum_{j > i}^4 \left(\frac{y_i + y_j}{2} - \bar{y}_N\right)^2
\\[10pt]
&= \frac{1}{16}\left[  \sum_{i = 1}^4 (y_i - \bar{y}_N)^2 + 2 \frac{3}{2}S^2\right]
\\[10pt]
&= \frac{1}{16}\left[  \sum_{i = 1}^4 (y_i - \bar{y}_N)^2 + 3\frac{1}{3} \sum_{i = 1}^{4} (y_i - \bar{y}_N)^2\right]
\\[10pt]
&= \frac{1}{16}\left[  \sum_{i = 1}^4 (y_i - \bar{y}_N)^2 + 3\frac{1}{3} \sum_{i = 1}^{4} (y_i - \bar{y}_N)^2\right]
\\[10pt]
&= \frac{1}{16}\left[  2 \sum_{i = 1}^4 (y_i - \bar{y}_N)^2\right]
\\[10pt]
&= \frac{1}{8}\sum_{i = 1}^4 (y_i - \bar{y}_N)^2
\\[10pt]
&= \frac{\sigma^2}{2}
\end{aligned}
$$

Finally, the relative efficiency of estimator (1.2.66) to estimator (1.2.71) is

$$
\frac{V(y_{pwr})}{V(\hat y_{new})} = \frac{\sigma^2/2}{\sigma^2/2} = 1.
$$
